package com.dean.test;

public abstract class DAOFactory {

	public abstract CityDAO getCityDAO();
	public abstract CountryDAO getCountryDAO();
	
	/**
	 * Static method for getting the correct DAOFactory
	 * 
	 * @param type
	 * @return
	 */
	public static DAOFactory getDAOFactory(StorageType type) {
		switch (type) {
		case MySQL:
			return new MySQLDAOFactory();
		default:
			return null;
		}
	}
	
}
