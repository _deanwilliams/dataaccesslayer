package com.dean.test;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import com.dean.transferobjects.Country;

public interface CountryDAO {

	public List<Country> getAllCountries() throws NamingException, SQLException;
	
}
