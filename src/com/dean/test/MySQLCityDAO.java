package com.dean.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import com.dean.transferobjects.City;
import com.dean.transferobjects.CityImpl;

public class MySQLCityDAO implements CityDAO {

	/**
	 * Get a list of all the Cities
	 * 
	 * @return List<City>
	 * @throws SQLException
	 * @throws NamingException
	 */
	@Override
	public List<City> getAllCities() throws NamingException, SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		StringBuffer query = new StringBuffer("select * from city");

		List<City> cities = null;

		try {
			conn = MySQLDAOFactory.createConnection();
			stmt = conn.prepareStatement(query.toString());

			rs = stmt.executeQuery();

			while (rs.next()) {
				City city = getCity(rs);
				if (city != null) {
					if (cities == null)
						cities = new ArrayList<City>();
					cities.add(city);
				}
			}
			rs.close();
			rs = null;
			stmt.close();
			stmt = null;
			conn.close();
			conn = null;
		} finally {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (stmt != null) {
				stmt.close();
				stmt = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		}

		return cities;
	}

	/**
	 * Get a City Object
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private City getCity(ResultSet rs) throws SQLException {
		int cityID = rs.getInt("city_id");
		String city = rs.getString("city");
		int countryID = rs.getInt("country_id");
		Date lastUpdate = new Date(rs.getTimestamp("last_update").getTime());
		CityImpl cityImpl = new CityImpl(cityID, city, countryID, lastUpdate);
		return cityImpl;
	}

}
