package com.dean.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import com.dean.transferobjects.Country;
import com.dean.transferobjects.CountryImpl;

public class MySQLCountryDAO implements CountryDAO {

	/**
	 * Get All Countries
	 * 
	 * @returns List<Country>
	 * @throws NamingException
	 * @throws SQLException
	 */
	@Override
	public List<Country> getAllCountries() throws NamingException, SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		StringBuffer query = new StringBuffer("select * from country");

		List<Country> countries = null;

		try {
			conn = MySQLDAOFactory.createConnection();
			stmt = conn.prepareStatement(query.toString());

			rs = stmt.executeQuery();

			while (rs.next()) {
				Country country = getCountry(rs);
				if (country != null) {
					if (countries == null)
						countries = new ArrayList<Country>();
					countries.add(country);
				}
			}
			rs.close();
			rs = null;
			stmt.close();
			stmt = null;
			conn.close();
			conn = null;
		} finally {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (stmt != null) {
				stmt.close();
				stmt = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		}

		return countries;
	}
	
	/**
	 * Populate Country Transfer Object
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Country getCountry(ResultSet rs) throws SQLException {
		int countryID = rs.getInt("country_id");
		String country = rs.getString("country");
		Date lastUpdate = new Date(rs.getTimestamp("last_update").getTime());
		CountryImpl countryImpl = new CountryImpl(countryID, country, lastUpdate);
		return countryImpl;
	}

}
