package com.dean.test;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;

import com.dean.transferobjects.City;

public interface CityDAO {

	public List<City> getAllCities() throws NamingException, SQLException;

}
