package com.dean.test;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class MySQLDAOFactory extends DAOFactory {

	/**
	 * Create a connection to the MySQL Datasource
	 * 
	 * @return
	 * @throws NamingException
	 * @throws SQLException
	 */
	public static Connection createConnection() throws NamingException,
			SQLException {
		// Get our context
		Context initCtx = new InitialContext();
		DataSource ds = (DataSource) initCtx.lookup("java:jdbc/SakilaDB");

		// Allocate and use a connection from the pool
		Connection conn = ds.getConnection();

		return conn;
	}

	/**
	 * Get the MySQL City DAO
	 * 
	 * @return CityDAO
	 */
	@Override
	public CityDAO getCityDAO() {
		return new MySQLCityDAO();
	}

	/**
	 * Get the MySQL Country DAO
	 * 
	 * @return CountryDAO
	 */
	@Override
	public CountryDAO getCountryDAO() {
		return new MySQLCountryDAO();
	}

}
